<?php

namespace EmailManager\Lib;

use Exception;
use PDOException;

class ErrorHandler {
    private const ERRORS = [
        400 => 'BadRequest',
        401 => 'Unauthorized',
        404 => 'ResourceNotFound',
        409 => 'Duplicate',
        500 => 'InternalServerError'
    ];

    /**
     * Handles errors and logs them to the console.
     * Should be used when an error occurs that usually should not happen (like a database connection error).
     * @param $exception Exception|string Exception to log.
     * @param bool $public Whether the exception should be sent to the client.
     */
    public static function throw($exception, $public = false) {
        $e = $exception;

        if (!$exception instanceof Exception)
            $e = new Exception($exception);

        http_response_code(500);
        error_log($e);
        $response = ['error' => $public ? $e->getTraceAsString() : self::ERRORS[500]];
        die(json_encode($response));
    }

    /**
     * Handles errors without logging them.
     * Should be used when something happens that might happen throughout the normal use of the API (like an SQL duplication error).
     * @param $code integer HTTP response status code.
     * @param $message string The error message that should be sent to the client. If null, a default message is going to be sent.
     */
    public static function handle ($code, $message = null) {
        http_response_code($code);
        $response = ['error' => $message == null ? self::ERRORS[$code] : $message];
        die(json_encode($response));
    }

    /**
     * Handles PDO exceptions.
     * @param $exception PDOException
     */
    public static function handlePDO ($exception) {
        switch ($exception->getCode()) {
            /** @noinspection PhpMissingBreakStatementInspection */
            case '23000':
                if (self::checkPDOIntegrityException($exception, '1062'))
                    self::handle(409);
                if (self::checkPDOIntegrityException($exception, '1048'))
                    self::handle(404);
                // Fallthrough intended
            default:
                self::throw($exception);
                break;
        }
    }

    /**
     * @param $exception Exception
     * @param $code integer|string
     * @return bool
     */
    private static function checkPDOIntegrityException ($exception, $code) {
        return strpos($exception->getMessage(), "Integrity constraint violation: $code") !== false;
    }
}