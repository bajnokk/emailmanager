<?php
namespace EmailManager\Lib;

class Helpers {
    private static function getBaseURL () {
        $scheme = $_SERVER['REQUEST_SCHEME'];
        $hostname = $_SERVER['SERVER_NAME'];
        $port = $_SERVER['SERVER_PORT'];
        $standardport = ($scheme === 'http' && $port === '80') || ($scheme === 'https' && $port === '443');

        return "$scheme://$hostname" . ($standardport ? '' : ":$port") . '/api';
    }

    public static function assembleURL ($endpoint) {
        $base = self::getBaseURL();
        $get = '';

        if (isset($_GET['apiKey'])) {
            $get .= '?apiKey=' . $_GET['apiKey'];
        }

        return $base . $endpoint . $get;
    }

    public static function parseEmail ($email) {
        $splt = explode('@', $email);

        if (count($splt) !== 2) {
            ErrorHandler::handle(400, 'Invalid email');
        }

        return [
            'username' => $splt[0],
            'domain' => $splt[1]
        ];
    }
}
