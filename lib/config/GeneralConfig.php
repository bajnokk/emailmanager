<?php
namespace EmailManager\Lib\Config;

class GeneralConfig extends Config {
    protected $location = 'general';
    protected $types = [
        'api_key' => self::STRING,
        'maildir' => self::STRING,
        'hash_salt' => self::STRING,
        'hash_workload_factor' => self::INTEGER
    ];

    public $api_key,
        $maildir,
        $hash_salt,
        $hash_workload_factor = 12;

    public function parse ($config) {
        $this->checkAndSet($config, 'api_key', $this->api_key);
        $this->checkAndSet($config, 'maildir', $this->maildir);
        $this->checkAndSet($config, 'hash_salt', $this->hash_salt);
        $this->checkAndSet($config, 'hash_workload_factor', $this->hash_workload_factor);
    }

    public function check () {
        $this->mustBeSet($this->api_key, 'api_key');
        $this->mustBeSet($this->maildir, 'maildir');
        $this->mustBeSet($this->hash_salt, 'hash_salt');

        if (strlen($this->hash_salt) != 22)
            $this->throwError('Option "hash_salt" must be of length 22.');
    }
}