<?php
namespace EmailManager\Lib\Config;

use EmailManager\Lib\ErrorHandler;
use Yosymfony\Toml\Exception\ParseException;
use Yosymfony\Toml\Toml;

class AppConfig extends Config {
    /** @var AppConfig $config */
    public static $config;

    public $database,
        $general,
        $hooks = [];

    public function __construct () {
        $this->database = new DBConfig();
        $this->general = new GeneralConfig();
    }

    /**
     * Get a Config instance from an array of potential configuration paths.
     * @param $locations string[] An array of configuration paths that should be checked.
     */
    public static function init ($locations) {
        foreach ($locations as $location)
            if (file_exists($location)) {
                $path = $location;
                break;
            }

        if (!isset($path))
            ErrorHandler::throw('No configuration file was provided.');

        $app_config = new AppConfig();
        $app_config->parseFile($path);
        $app_config->check();

        self::$config = $app_config;
    }

    /**
     * Parses a configuration file.
     * @param $path string
     */
    public function parseFile ($path) {
        try {
            $config = Toml::parseFile($path);
        } catch (ParseException $e) {
            ErrorHandler::throw($e);
        }

        if (!isset($config))
            $this->throwError('Configuration file ":path" could not be parsed. Check the syntax.', [':path' => $path]);

        if (isset($config['include']))
            foreach ($config['include'] as $include) {
                $this->mustBeSet($include['file'], 'file', null, 'include');
                $this->enforceType($include['file'], 'file', 'string', 'include');

                $include_path = $include['file'];

                if (!file_exists($include_path))
                    $this->throwError('Included file ":include_path" does not exist.', ['include_path' => $include_path]);

                $this->parseFile($include_path);
            }

        $this->parse($config);
    }

    public function parse ($config) {
        if (isset($config['database']))
            $this->database->parse($config['database']);

        if (isset($config['general']))
            $this->general->parse($config['general']);

        if (isset($config['hook']))
            foreach ($config['hook'] as $hook) {
                $hook_config = new HookConfig();
                $hook_config->parse($hook);

                array_push($this->hooks, $hook_config);
            }
    }

    public function check () {
        $this->database->check();
        $this->general->check();

        /** @var HookConfig $hook */
        foreach ($this->hooks as $hook) {
            $hook->check();
        }
    }
}