<?php
namespace EmailManager\Lib\Config;

use EmailManager\Lib\ErrorHandler;

abstract class Config {
    /**
     * These variables should be used to set option types in $location.
     */
    const BOOLEAN = 'boolean',
        INTEGER = 'integer',
        STRING = 'string';

    /**
     * @var $location string Should be set to the configuration location appearing in potential error logs.
     */
    protected $location;
    /**
     * @var $types array Contains key-value-pairs with the key being a configuration option and the value being the type this option must have.
     */
    protected $types = [];

    /**
     * Should Config->checkAndSet() all member variables to the respective configuration option.
     * @param $config array The configuration array.
     */
    public abstract function parse ($config);

    /**
     * Should check whether all required member variables are set using Config->mustBeSet().
     * Conditional requirements can be achieved using if statements and setting the $condition parameter in Config->mustBeSet().
     */
    public abstract function check ();

    /**
     * Checks if a variable exists in the configuration array, checks for its type and sets the respective member variable.
     * @param $config array The configuration array.
     * @param $name string The key of the option in $config.
     * @param $var mixed A reference to the variable that should be set if conditions are met.
     */
    protected function checkAndSet ($config, $name, &$var) {
        if (!isset($config[$name]))
            return;

        $option = $config[$name];
        $this->enforceType($option, $name, $this->types[$name]);

        $var = $option;
    }

    /**
     * Enforces the existence of a variable. Primarily used in Config->check() methods.
     * @param $option mixed A reference to the variable to be checked.
     * @param $name string The name of the configuration option for logging.
     * @param $condition string If the variable must only be existent under a certain condition, it might be specified here. This string will be inserted into: "... is required when {condition}".
     * @param $custom_location string In case the location that should appear in the logs differs from the $location member, it might be specified here.
     */
    protected function mustBeSet (&$option, $name, $condition = null, $custom_location = null) {
        if (!isset($option))
            $this->throwError('Option ":option" is required:condition.', [
                ':option' => $name,
                ':condition' => $condition == null ? '' : ' when ' . $condition
            ], $custom_location);
    }

    /**
     * Enforces a certain type on a variable.
     * @param $option mixed A reference to the variable to be checked.
     * @param $name string The name of the configuration option for logging.
     * @param $type string The type of the variable that must be matched.
     * @param $custom_location string In case the location that should appear in the logs differs from the $location member, it might be specified here.
     */
    protected function enforceType (&$option, $name, $type, $custom_location = null) {
        if (gettype($option) != $type)
            $this->throwError('":option" must be of type ":type"', [
                ':option' => $name,
                ':type' => $type
            ], $custom_location);
    }

    /**
     * Logs an error replacing :parameters using strtr().
     * @param $error string The error message to be logged.
     * @param $params array An array containing :parameters that should be replaced. Usage: [ ":param" => "Replaced by this" ].
     * @param $custom_location string In case the location that should appear in the logs differs from the $location member, it might be specified here.
     */
    protected function throwError ($error, $params = null, $custom_location = null) {
        ErrorHandler::throw('ConfigError: '
            . ((isset($this->location) || isset($custom_location))
                ? '[' . ($custom_location ?? $this->location) . '] '
                : '')
            . strtr($error, $params ?? []));
    }
}