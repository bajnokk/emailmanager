<?php
namespace EmailManager\Lib\Config;

class DBConfig extends Config {
    protected $location = 'database';
    protected $types = [
        'driver' => self::STRING,
        'host' => self::STRING,
        'port' => self::INTEGER,
        'dbname' => self::STRING,
        'username' => self::STRING,
        'password' => self::STRING,
        'file' => self::STRING
    ];

    public $driver = 'mysql',
        $host = '127.0.0.1',
        $port = 3306,
        $dbname = 'hosting',
        $username,
        $password,
        $file;

    public function parse($dbconfig) {
        $this->checkAndSet($dbconfig, 'driver', $this->driver);
        $this->checkAndSet($dbconfig, 'host', $this->host);
        $this->checkAndSet($dbconfig, 'port', $this->port);
        $this->checkAndSet($dbconfig, 'dbname', $this->dbname);
        $this->checkAndSet($dbconfig, 'username', $this->username);
        $this->checkAndSet($dbconfig, 'password', $this->password);
        $this->checkAndSet($dbconfig, 'file', $this->file);
    }

    public function check() {
        if ($this->driver == 'sqlite') {
            $this->mustBeSet($this->file, 'file', 'driver is sqlite');
            return;
        }

        $this->mustBeSet($this->username, 'username', 'driver is not sqlite');
        $this->mustBeSet($this->password, 'password', 'driver is not sqlite');
    }
}