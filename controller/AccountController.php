<?php
namespace EmailManager\Controller;

use EmailManager\Lib\Config\AppConfig;
use EmailManager\Lib\ErrorHandler;
use EmailManager\Lib\Helpers;
use EmailManager\Models\AliasModel;
use EmailManager\Models\DomainModel;
use EmailManager\Models\ForwardModel;
use Flight;
use EmailManager\Models\AccountModel;

class AccountController {
    public static function getAll () {
        $group = isset($_GET['group']) ? $_GET['group'] : 'account';
        if (!in_array($group, ['domain', 'account'])) {
            $group = 'account';
        }

        if ($group === 'account') {
            $response = array_map(function ($account) {
                return assembleResponseForAccount($account);
            }, AccountModel::getAll());

            echo json_encode($response);
        } else {
            $response = array_map(function ($domain) {
                $accounts = array_map(function ($account) {
                    return assembleResponseForAccount($account);
                }, AccountModel::getAllForDomain($domain->name));

                return [
                    'domain' => $domain->name,
                    'href' => Helpers::assembleURL("/domains/$domain->name"),
                    'accounts' => $accounts
                ];
            }, DomainModel::getAll());

            echo json_encode($response);
        }
    }

    public static function get ($email) {
        $email_data = Helpers::parseEmail($email);
        $account = AccountModel::getSingle($email_data['domain'], $email_data['username']);
        $response = assembleResponseForAccount($account, true, true);

        echo json_encode($response);
    }

    public static function delete ($email) {
        $email_data = Helpers::parseEmail($email);

        if (AccountModel::getSingle($email_data['domain'], $email_data['username']) == null)
            ErrorHandler::handle(404);

        AccountModel::delete($email_data['domain'], $email_data['username']);

        echo RESULT_OK;
    }

    public static function addAlias ($email) {
        $data = Flight::request()->data;
        $email_data = Helpers::parseEmail($email);

        foreach (AliasModel::FIELDS as $key => $value) {
            if (!isset($data[$key]) || gettype($data[$key]) != $value)
                ErrorHandler::handle(400);
        }

        $alias = new AliasModel();
        $alias->email = $data['email'];

        $alias->add($email_data['domain'], $email_data['username']);

        echo RESULT_OK;
    }

    public static function deleteAlias ($email, $alias) {
        $email_data = Helpers::parseEmail($email);

        if (AliasModel::getSingle($email_data['domain'], $email_data['username'], $alias) == null)
            ErrorHandler::handle(404);

        AliasModel::delete($email_data['domain'], $email_data['username'], $alias);

        echo RESULT_OK;
    }

    public static function addForward ($email) {
        $data = Flight::request()->data;
        $email_data = Helpers::parseEmail($email);

        foreach (ForwardModel::FIELDS as $key => $value) {
            if (!isset($data[$key]) || gettype($data[$key]) != $value) {
                ErrorHandler::handle(400);
            }
        }

        $forward = new ForwardModel();
        $forward->forward_address = $data['forward_address'];

        $forward->add($email_data['domain'], $email_data['username']);

        echo RESULT_OK;
    }

    public static function deleteForward ($email, $forward) {
        $email_data = Helpers::parseEmail($email);

        if (ForwardModel::getSingle($email_data['domain'], $email_data['username'], $forward) == null) {
            ErrorHandler::handle(404);
        }

        ForwardModel::delete($email_data['domain'], $email_data['username'], $forward);

        echo RESULT_OK;
    }
}

function assembleResponseForAccount ($account, $with_aliases = false, $with_forwards = false) {
    $email = "$account->username@$account->domain_name";
    $response = [
        'id' => $account->id,
        'username' => $account->username,
        'domain' => $account->domain_name,
        'access_mail' => $account->access_mail,
        'receive_mail' => $account->receive_mail,
        'create_time' => $account->create_time,
        'update_time' => $account->update_time,
        'quota' => $account->quota,
        'email' => $email,
        'href' => Helpers::assembleURL("/accounts/$email"),
    ];

    if ($with_aliases) {;
        $response['aliases'] = AliasModel::getAll($account->domain_name, $account->username);
    }

    if ($with_forwards) {
        $response['forwards'] = ForwardModel::getAll($account->domain_name, $account->username);
    }

    return $response;
}
