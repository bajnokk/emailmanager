<?php
namespace EmailManager\Controller;

use EmailManager\Lib\Config\AppConfig;
use EmailManager\Models\AccountModel;
use EmailManager\Models\DomainModel;
use EmailManager\Models\DomainAdminModel;
use EmailManager\Lib\ErrorHandler;
use EmailManager\Lib\Helpers;
use Flight;

class DomainController {
    public static function get () {
        $response = array_map(function ($domain) {
            return assembleResponseForDomain($domain);
        }, DomainModel::getAll());

        echo json_encode($response);
    }

    public static function getSingle ($domain_name) {
        $domain = DomainModel::getSingle($domain_name);
        $response = assembleResponseForDomain($domain, true);
        echo json_encode($response);
    }

    public static function create () {
        $data = Flight::request()->data;

        foreach (DomainModel::FIELDS as $key => $value) {
            if (!isset($data[$key]) || gettype($data[$key]) != $value)
                ErrorHandler::handle(400);
        }

        $domain = new DomainModel();
        $domain->name = $data['name'];
        $domain->quota = $data['quota'];

        $domain->add();

        echo RESULT_OK;
    }

    public static function update ($domain_name) {
        $data = Flight::request()->data;

        foreach (DomainModel::FIELDS as $key => $value) {
            if (isset($data[$key]) && gettype($data[$key]) != $value)
                ErrorHandler::handle(400);
        }

        $domain = DomainModel::getSingle($domain_name);

        if ($domain == null)
            ErrorHandler::handle(404);

        foreach ($data as $key => $value) {
            switch ($key) {
                case 'name':
                    $domain->name = $value;
                    break;
                case 'quota':
                    $domain->quota = $value;
                    break;
            }
        }

        $domain->update($domain_name);

        echo RESULT_OK;
    }

    public static function delete ($domain) {
        if (DomainModel::getSingle($domain) == null)
            ErrorHandler::handle(404);

        DomainModel::delete($domain);

        echo RESULT_OK;
    }

    public static function createAccount ($domain) {
        $data = Flight::request()->data;

        foreach (AccountModel::FIELDS as $key => $value) {
            if (!isset($data[$key]) || gettype($data[$key]) != $value)
                ErrorHandler::handle(400);
        }

        $account = new AccountModel();
        $account->username = $data['username'];
        $account->access_mail = $data['access_mail'];
        $account->receive_mail = $data['receive_mail'];
        $account->quota = $data['quota'];
        $account->setPassword($data['password'], AppConfig::$config->general);

        $account->add($domain);

        echo RESULT_OK;
    }
}

function assembleResponseForDomain ($domain, $with_accounts = false) {
    $response = [
        'id' => $domain->id,
        'name' => $domain->name,
        'quota' => $domain->quota,
        'href' => Helpers::assembleURL("/domains/$domain->name")
    ];

    $response['admins'] = array_map(function ($admin) {
        return [
            'id' => $admin->id,
            'name' => $admin->name,
            'username' => $admin->username,
            'enabled' => $admin->enabled === 1,
            'create_time' => $admin->create_time,
            'update_time' => $admin->update_time,
            'href' => Helpers::assembleURL("/admins/$admin->id")
        ];
    }, DomainAdminModel::getAllForDomain($domain->name));

    if ($with_accounts) {
        $response['accounts'] = array_map(function ($account) {
            $email = "$account->username@$account->domain_name";

            return [
                'id' => $account->id,
                'username' => $account->username,
                'email' => $email,
                'href' => Helpers::assembleURL("/accounts/$email")
            ];
        }, AccountModel::getAllForDomain($domain->name));
    }

    return $response;
}
