<?php
namespace EmailManager\Models;

use EmailManager\Lib\Config\GeneralConfig;
use EmailManager\Lib\Database;
use EmailManager\Lib\ErrorHandler;
use EmailManager\Lib\Hooks;

class AccountModel {
    public const FIELDS = [
        'username' => 'string',
        'password' => 'string',
        'access_mail' => 'boolean',
        'receive_mail' => 'boolean',
        'quota' => 'integer'
    ];

    public $id, $username, $access_mail, $receive_mail, $create_time, $update_time, $quota, $domain_name;
    private $password;

    /**
     * Hashes and sets the email account password.
     * @param $password string
     * @param $config GeneralConfig
     */
    public function setPassword($password, $config) {
        $this->password = $this->hash($password, $config);
    }

    /**
     * Hashes a password using the provided salt and workload factor.
     * @param $password string
     * @param $config GeneralConfig
     * @return string
     */
    private function hash ($password, $config) {
        return crypt($password, '$2y$' . $config->hash_workload_factor . '$' . $config->hash_salt);
    }

    /**
     * Fetches all email accounts on a specific domain.
     * @param $domain string
     * @return AccountModel[]
     */
    public static function getAllForDomain ($domain) {
        $query = <<<EOD
                    SELECT email_account.id,
                           email_account.username,
                           email_account.password,
                           email_account.access_mail,
                           email_account.receive_mail,
                           email_account.create_time,
                           email_account.update_time,
                           email_account.quota,
                           email_domain.name AS domain_name
                    FROM email_account, email_domain
                    WHERE email_account.domain_id = email_domain.id
                      AND email_domain.name = :domain
                EOD;

        return Database::fetchAllObj($query, self::class, ['domain' => $domain]);
    }

    public static function getAll () {
        $query = <<<EOD
                    SELECT email_account.id,
                           email_account.username,
                           email_account.password,
                           email_account.access_mail,
                           email_account.receive_mail,
                           email_account.create_time,
                           email_account.update_time,
                           email_account.quota,
                           email_domain.name AS domain_name
                    FROM email_account, email_domain
                    WHERE email_account.domain_id = email_domain.id
                    EOD;

        return Database::fetchAllObj($query, self::class);
    }

    /**
     * Fetches a single email account with provided username on a specific domain.
     * @param $domain string
     * @param $username string
     * @return AccountModel
     */
    public static function getSingle ($domain, $username) {
        $query = <<<EOD
                    SELECT email_account.id,
                           email_account.username,
                           email_account.password,
                           email_account.access_mail,
                           email_account.receive_mail,
                           email_account.create_time,
                           email_account.update_time,
                           email_account.quota,
                           email_domain.name AS domain_name
                    FROM email_account, email_domain
                    WHERE email_account.domain_id = email_domain.id
                      AND email_account.username = :username
                      AND email_domain.name = :domain
                EOD;

        return Database::fetchSingleObj($query, self::class, [
            'username' => $username,
            'domain' => $domain,
        ]);
    }

    /**
     * Deletes an email account from the database.
     * @param $domain string
     * @param $username string
     */
    public static function delete ($domain, $username) {
        Hooks::runPreDelete(Hooks::ACCOUNT, [
            'username' => $username,
            'domain' => $domain
        ]);

        $query = <<<EOD
                    DELETE email_account
                    FROM email_account,
                         email_domain
                    WHERE email_account.domain_id = email_domain.id
                      AND email_account.username = :username
                      AND email_domain.name = :domain
                EOD;

        Database::set($query, [
            'username' => $username,
            'domain' => $domain
        ]);

        Hooks::runPostDelete(Hooks::ACCOUNT, [
            'username' => $username,
            'domain' => $domain
        ]);
    }

    /**
     * Adds a new email account to the database.
     * @param $domain string
     */
    public function add ($domain) {
        Hooks::runPreCreate(Hooks::ACCOUNT, [
            'username' => $this->username,
            'access_mail' => $this->access_mail,
            'receive_mail' => $this->receive_mail,
            'domain' => $domain,
            'quota' => $this->quota
        ]);

        $query = <<<EOD
                    INSERT INTO email_account (username,
                                               password,
                                               access_mail,
                                               receive_mail,
                                               domain_id,
                                               create_time,
                                               update_time,
                                               quota)
                    VALUES (:username,
                            :password,
                            :access_mail,
                            :receive_mail,
                            (SELECT id FROM email_domain WHERE name = :domain),
                            CURRENT_TIME(),
                            CURRENT_TIME(),
                            :quota)
                 EOD;

        if ($this->checkDuplicate($domain))
            ErrorHandler::handle(409);

        Database::set($query, [
            'username' => $this->username,
            'password' => $this->password,
            'access_mail' => $this->access_mail,
            'receive_mail' => $this->receive_mail,
            'domain' => $domain,
            'quota' => $this->quota
        ]);

        Hooks::runPostCreate(Hooks::ACCOUNT, [
            'username' => $this->username,
            'access_mail' => $this->access_mail,
            'receive_mail' => $this->receive_mail,
            'domain' => $domain,
            'quota' => $this->quota
        ]);
    }

    /**
     * Updates a user in the database.
     * @param $domain string
     * @param $old_username string
     */
    public function update ($domain, $old_username) {
        Hooks::runPreUpdate(Hooks::ACCOUNT, [
            'username' => $this->username,
            'access_mail' => $this->access_mail,
            'receive_mail' => $this->receive_mail,
            'domain' => $domain,
            'quota' => $this->quota
        ]);

        $query = <<<EOD
                    UPDATE email_account, email_domain
                    SET email_account.username = :username,
                        email_account.access_mail = :access_mail,
                        email_account.receive_mail = :receive_mail,
                        email_account.update_time = CURRENT_TIME(),
                        email_account.password = :password,
                        email_account.quota = :quota
                    WHERE email_account.domain_id = email_domain.id
                      AND email_account.username = :old_username
                      AND email_domain.name = :domain
                EOD;

        if ($old_username != $this->username && $this->checkDuplicate($domain))
            ErrorHandler::handle(409);

        Database::set($query, [
            'username' => $this->username,
            'password' => $this->password,
            'access_mail' => $this->access_mail,
            'receive_mail' => $this->receive_mail,
            'domain' => $domain,
            'quota' => $this->quota,
            'old_username' => $old_username
        ]);

        Hooks::runPostUpdate(Hooks::ACCOUNT, [
            'username' => $this->username,
            'access_mail' => $this->access_mail,
            'receive_mail' => $this->receive_mail,
            'domain' => $domain,
            'quota' => $this->quota
        ]);
    }

    /**
     * Checks whether an account with specified username-domain-combination already exists.
     * @param $domain string
     * @return bool
     */
    private function checkDuplicate ($domain) {
        $query = <<<EOD
                    SELECT COUNT(email_account.username) AS count
                    FROM email_account, email_domain
                    WHERE email_domain.id = email_account.domain_id
                      AND email_domain.name = :domain
                      AND email_account.username = :username
                EOD;

        $res = Database::fetch($query, [ 'domain' => $domain, 'username' => $this->username ]);

        return $res['count'] > 0;
    }
}