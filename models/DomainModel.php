<?php

namespace EmailManager\Models;

use EmailManager\Lib\Database;
use EmailManager\Lib\ErrorHandler;
use EmailManager\Lib\Hooks;

class DomainModel {
    public const FIELDS = [
        'name' => 'string',
        'quota' => 'integer'
    ];

    public $id, $name, $quota;

    /**
     * Fetches a list of all available domains.
     * @return DomainModel[]
     */
    public static function getAll () {
        $query = <<<EOD
                        SELECT id, name, quota
                        FROM email_domain
                    EOD;

        return Database::fetchAllObj($query, self::class);
    }

    /**
     * Fetches a single domain.
     * @param $domain string
     * @return DomainModel
     */
    public static function getSingle ($domain) {
        $query = <<<EOD
                        SELECT id, name, quota
                        FROM email_domain
                        WHERE email_domain.name = :domain
                    EOD;

        return Database::fetchSingleObj($query, self::class, ['domain' => $domain]);
    }

    /**
     * Adds a new domain.
     */
    public function add () {
        Hooks::runPreCreate(Hooks::DOMAIN, [
            'name' => $this->name,
            'quota' => $this->quota
        ]);

        $query = <<<EOD
                        INSERT INTO email_domain (name,
                                                  quota)
                        VALUES (:name,
                                :quota)
                    EOD;

        if ($this->checkDuplicate())
            ErrorHandler::handle(409);

        Database::set($query, [
            'name' => $this->name,
            'quota' => $this->quota
        ]);

        Hooks::runPostCreate(Hooks::DOMAIN, [
            'name' => $this->name,
            'quota' => $this->quota
        ]);
    }

    /**
     * Updates a domain in the database.
     * @param $old_name string
     */
    public function update ($old_name) {
        Hooks::runPreUpdate(Hooks::DOMAIN, [
            'name' => $this->name,
            'quota' => $this->quota
        ]);

        $query = <<<EOD
                        UPDATE email_domain
                        SET email_domain.name = :name,
                            email_domain.quota = :quota
                        WHERE email_domain.name = :old_name
                    EOD;

        if ($old_name != $this->name && $this->checkDuplicate())
            ErrorHandler::handle(409);

        Database::set($query, [
            'name' => $this->name,
            'quota' => $this->quota,
            'old_name' => $old_name
        ]);

        Hooks::runPostUpdate(Hooks::DOMAIN, [
            'name' => $this->name,
            'quota' => $this->quota
        ]);
    }

    /**
     * Deletes a domain from the database.
     * The database must be configured so that rows in other tables referencing this domain in get automatically deleted as well!
     * @param $domain string
     */
    public static function delete ($domain) {
        Hooks::runPreDelete(Hooks::DOMAIN, [
            'domain' => $domain
        ]);

        $query = <<<EOD
                        DELETE email_domain
                        FROM email_domain
                        WHERE email_domain.name = :domain
                    EOD;

        Database::set($query, [ 'domain' => $domain ]);

        Hooks::runPostDelete(Hooks::DOMAIN, [
            'domain' => $domain
        ]);
    }

    /**
     * Checks whether a domain with specified name already exists.
     * @return bool
     */
    private function checkDuplicate () {
        $query = <<<EOD
                        SELECT COUNT(email_domain.name) AS count
                        FROM email_domain
                        WHERE email_domain.name = :domain
                    EOD;

        $res = Database::fetch($query, ['domain' => $this->name]);

        return $res['count'] > 0;
    }
}