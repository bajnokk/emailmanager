<?php
namespace EmailManager\Models;

use EmailManager\Lib\Database;
use EmailManager\Lib\ErrorHandler;
use EmailManager\Lib\Hooks;

class AliasModel {
    public const FIELDS = [
        'email' => 'string'
    ];

    public $email;

    /**
     * Gets all aliases for an email account.
     * @param $domain string
     * @param $account string
     * @return string[]
     */
    public static function getAll ($domain, $account) {
        $query = <<<EOD
                        SELECT email_alias.email
                        FROM email_alias, email_domain, email_account
                        WHERE email_account.username = :account
                          AND email_domain.name = :domain
                          AND email_account.id = email_alias.account_id
                          AND email_domain.id = email_account.domain_id
                    EOD;

        return Database::fetchAll($query, [
            'account' => $account,
            'domain' => $domain
        ], true);
    }

    /**
     * Gets a single email alias.
     * @param $domain string
     * @param $account string
     * @param $alias string
     * @return AliasModel
     */
    public static function getSingle ($domain, $account, $alias) {
        $query = <<<EOD
                        SELECT email_alias.email
                        FROM email_alias, email_domain, email_account
                        WHERE email_alias.email = :alias
                          AND email_account.username = :account
                          AND email_domain.name = :domain
                          AND email_account.id = email_alias.account_id
                          AND email_domain.id = email_account.domain_id
                    EOD;

        return Database::fetchSingleObj($query, self::class, [
            'alias' => $alias,
            'account' => $account,
            'domain' => $domain
        ]);
    }

    /**
     * Deletes an alias from the database.
     * @param $domain string
     * @param $account string
     * @param $alias string
     */
    public static function delete ($domain, $account, $alias) {
        Hooks::runPreDelete(Hooks::ALIAS, [
            'domain' => $domain,
            'account' => $account,
            'alias' => $alias
        ]);

        $query = <<<EOD
                        DELETE email_alias
                        FROM email_alias, email_domain, email_account
                        WHERE email_domain.name = :domain
                          AND email_account.username = :account
                          AND email_alias.email = :alias
                          AND email_account.domain_id = email_domain.id
                          AND email_alias.account_id = email_account.id
                    EOD;

        Database::set($query, [
            'domain' => $domain,
            'account' => $account,
            'alias' => $alias
        ]);

        Hooks::runPostDelete(Hooks::ALIAS, [
            'domain' => $domain,
            'account' => $account,
            'alias' => $alias
        ]);
    }

    /**
     * Add new alias for an email account.
     * @param $domain string
     * @param $account string
     */
    public function add ($domain, $account) {
        Hooks::runPreCreate(Hooks::ALIAS, [
            'domain' => $domain,
            'account' => $account,
            'alias' => $this->email
        ]);

        $query = <<<EOD
                        INSERT INTO email_alias (email,
                                                 account_id) 
                        VALUES (:email, (
                                  SELECT email_account.id
                                  FROM email_account, email_domain
                                  WHERE email_account.username = :account
                                    AND email_domain.name = :domain
                                    AND email_domain.id = email_account.domain_id
                                ))
                    EOD;

        if ($this->checkDuplicate($domain, $account))
            ErrorHandler::handle(409);

        Database::set($query, [
            'email' => $this->email,
            'account' => $account,
            'domain' => $domain
        ]);

        Hooks::runPostCreate(Hooks::ALIAS, [
            'domain' => $domain,
            'account' => $account,
            'alias' => $this->email
        ]);
    }

    /**
     * Checks whether an alias with specified alias-username-domain-combination already exists.
     * @param $domain string
     * @param $account string
     * @return bool
     */
    private function checkDuplicate ($domain, $account) {
        $query = <<<EOD
                        SELECT COUNT(email_alias.email) AS count
                        FROM email_alias, email_domain, email_account
                        WHERE email_domain.name = :domain
                          AND email_account.username = :account
                          AND email_alias.email = :alias
                          AND email_account.domain_id = email_domain.id
                          AND email_alias.account_id = email_account.id
                    EOD;

        $res = Database::fetch($query, [
            'domain' => $domain,
            'account' => $account,
            'alias' => $this->email
        ]);

        return $res['count'] > 0;
    }
}