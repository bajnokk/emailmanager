<?php
namespace EmailManager\Models;

use EmailManager\Lib\Database;
use EmailManager\Lib\ErrorHandler;
use EmailManager\Lib\Hooks;

class ForwardModel {
    public const FIELDS = [
        'forward_address' => 'string'
    ];

    public $forward_address;

    /**
     * Gets all forwards for an email account.
     * @param $domain string
     * @param $account string
     * @return string[]
     */
    public static function getAll ($domain, $account) {
        $query = <<<EOD
                        SELECT email_forward.forward_address
                        FROM email_forward, email_domain, email_account
                        WHERE email_account.username = :account
                          AND email_domain.name = :domain
                          AND email_account.id = email_forward.account_id
                          AND email_domain.id = email_account.domain_id
                    EOD;

        return Database::fetchAll($query, [
            'account' => $account,
            'domain' => $domain
        ], true);
    }

    /**
     * Gets a single email forward.
     * @param $domain string
     * @param $account string
     * @param $forward string
     * @return ForwardModel
     */
    public static function getSingle ($domain, $account, $forward) {
        $query = <<<EOD
                        SELECT email_forward.forward_address
                        FROM email_forward, email_domain, email_account
                        WHERE email_forward.forward_address= :forward
                          AND email_account.username = :account
                          AND email_domain.name = :domain
                          AND email_account.id = email_forward.account_id
                          AND email_domain.id = email_account.domain_id
                    EOD;

        return Database::fetchSingleObj($query, self::class, [
            'forward' => $forward,
            'account' => $account,
            'domain' => $domain
        ]);
    }

    /**
     * Deletes a forward address from the database.
     * @param $domain string
     * @param $account string
     * @param $forward string
     */
    public static function delete ($domain, $account, $forward) {
        Hooks::runPreDelete(Hooks::FORWARD, [
            'domain' => $domain,
            'account' => $account,
            'forward' => $forward
        ]);

        $query = <<<EOD
                        DELETE email_forward
                        FROM email_forward, email_domain, email_account
                        WHERE email_domain.name = :domain
                          AND email_account.username = :account
                          AND email_forward.forward_address = :forward
                          AND email_account.domain_id = email_domain.id
                          AND email_forward.account_id = email_account.id
                    EOD;

        Database::set($query, [
            'domain' => $domain,
            'account' => $account,
            'forward' => $forward
        ]);

        Hooks::runPostDelete(Hooks::FORWARD, [
            'domain' => $domain,
            'account' => $account,
            'forward' => $forward
        ]);
    }

    /**
     * Adds a new forward address for an email account.
     * @param $domain string
     * @param $account string
     */
    public function add ($domain, $account) {
        Hooks::runPreCreate(Hooks::FORWARD, [
            'domain' => $domain,
            'account' => $account,
            'forward' => $this->forward_address
        ]);

        $query = <<<EOD
                        INSERT INTO email_forward (forward_address,
                                                 account_id) 
                        VALUES (:forward, (
                                  SELECT email_account.id
                                  FROM email_account, email_domain
                                  WHERE email_account.username = :account
                                    AND email_domain.name = :domain
                                    AND email_domain.id = email_account.domain_id
                                ))
                    EOD;

        if ($this->checkDuplicate($domain, $account))
            ErrorHandler::handle(409);

        Database::set($query, [
            'forward' => $this->forward_address,
            'account' => $account,
            'domain' => $domain
        ]);

        Hooks::runPostCreate(Hooks::FORWARD, [
            'domain' => $domain,
            'account' => $account,
            'forward' => $this->forward_address
        ]);
    }

    /**
     * Checks whether a forward address with specified forward-username-domain-combination already exists.
     * @param $domain string
     * @param $account string
     * @return bool
     */
    private function checkDuplicate ($domain, $account) {
        $query = <<<EOD
                        SELECT COUNT(email_forward.forward_address) AS count
                        FROM email_forward, email_domain, email_account
                        WHERE email_domain.name = :domain
                          AND email_account.username = :account
                          AND email_forward.forward_address = :forward
                          AND email_account.domain_id = email_domain.id
                          AND email_forward.account_id = email_account.id
                    EOD;

        $res = Database::fetch($query, [
            'domain' => $domain,
            'account' => $account,
            'forward' => $this->forward_address
        ]);

        return $res['count'] > 0;
    }
}