<?php
namespace EmailManager\Models;
/*
 * id int
 * name string
 * username string
 * password string
 * enabled boolean
 * create_time timestamp
 * update_time timestamp
 * */

use EmailManager\Lib\Database;

class DomainAdminModel {
    public const FIELDS = [
        'name' => 'string',
        'username' => 'string',
        'password' => 'string',
        'enabled' => 'boolean'
    ];

    public $id, $name, $username, $password, $enabled, $create_time, $update_time;

    public static function getAll () {
        $query = <<<EOD
                        SELECT name, username, password, enabled, create_time, update_time
                        FROM domain_admin
                    EOD;

        return Database::fetchAllObj($query, self::class);
    }

    public static function getAllForDomain ($domain) {
        $query = <<<EOD
                        SELECT domain_admin.id,
                               domain_admin.name,
                               domain_admin.username,
                               domain_admin.password,
                               domain_admin.enabled,
                               domain_admin.create_time,
                               domain_admin.update_time
                        FROM domain_admin, email_domain_has_domain_admin, email_domain
                        WHERE email_domain_has_domain_admin.domain_admin_id = domain_admin.id
                          AND email_domain_has_domain_admin.email_domain_id = email_domain.id
                          AND email_domain.name = :domain
                    EOD;

        return Database::fetchAllObj($query, self::class, ['domain' => $domain]);
    }
}
